<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class LoginController extends Controller 
{
    public function showForm() {
        return view('login');
    }

    public function loginCompany() {
        $user = array( 
            'email' => Request::get('email'),
            'password' => Request::get('password'),
        );
        if (Auth::attempt($user)) { //if the login was successful redirect user to homepages
            return Redirect::intended('/ayyy'); //if unsuccessful redirects them back.
        } else {
            return redirect('/failedG')->withErrors([
                'error' => 'Wrong username or Password',
            ]);

        }
    }
}
