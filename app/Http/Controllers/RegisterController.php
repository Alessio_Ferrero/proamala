<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\CompInfo;
use Input;
use Hash;

class RegisterController extends Controller
{
    public function showCompanyForm() {

        return view('register');
    }

    public function registerCompany() {

        $input = new CompInfo();

        $input->company_name = Input::get('companyName');
        $input->email = Input::get('email');
        $input->password = Hash::make(Input::get('password'));
        $input->address_line_1 = Input::get('addrline1');
        $input->address_line_2 = Input::get('addrline2');
        $input->city = Input::get('city');
        $input->postcode = Input::get('postcode');
        $input->phone_number = Input::get('phoneNumber');
        $input->website = Input::get('url');
        $input->description = Input::get('description');

        $input->save();

        return Redirect::to('/login')->withErrors([
                'error' => 'Company Account Created',
            ]);
    }
}
