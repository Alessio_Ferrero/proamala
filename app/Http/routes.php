<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('register', 'RegisterController@showCompanyForm');

Route::post('register', 'RegisterController@registerCompany');

Route::get('login', 'LoginController@showForm');

Route::post('login', 'LoginController@loginCompany');