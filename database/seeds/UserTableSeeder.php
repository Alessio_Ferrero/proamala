<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'email' => 'alessioferrero@mail.com',
            'username' => 'alessio',
            'password' => Hash::make('Alessio654654@'),
            'permission' => 1,
        ]);
    }
}
