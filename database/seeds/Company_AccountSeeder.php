<?php

use Illuminate\Database\Seeder;

class Company_AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('company_account')->insert([
    		'company_name' => 'Test Company',
        	'email' => 'alessioferrero@mail.com',
            'password' => Hash::make('Alessio654654@'),
            'address_line_1' => '70 kay street',
            'address_line_2' => 'Stalybridge',
            'city' => 'Manchester',
            'postcode' => 'SK15 2EH',
            'phone_number' => '07453756677',
            'website' => 'http://www.alessioferrero.com',
            'description' => 'Ay man fuck you man',
        ]);
    }
}
