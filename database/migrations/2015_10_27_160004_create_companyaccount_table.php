<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyaccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 64);
            $table->string('email', 64)->unique();
            $table->string('password', 256);
            $table->string('address_line_1', 256)->nullable();
            $table->string('address_line_2', 256)->nullable();
            $table->string('city', 256)->nullable();
            $table->string('postcode', 16)->nullable();
            $table->string('phone_number', 16)->nullable();
            $table->string('website', 256)->nullable();
            $table->string('description', 256)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_account');
    }
}