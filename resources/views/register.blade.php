<!DOCTYPE html>
<html>
  <head>
    <title>Proamala-HomePage</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    {!!HTML::style('css/font-awesome.min.css')!!}
    {!!HTML::style('css/bootstrap.min.css')!!}
    {!!HTML::style('css/bootstrap-form-helpers.min.css')!!}
    {!!HTML::style('css/createAccount.css')!!}

  </head>

  <body>

    <!-- REGISTER -->
    <div class="container content">
      <div class="row">
        <div class="container col-sm-6">
        {!! Form::open(array('url' => 'register', 'class' => 'form-horizontal')) !!}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <form class="form-horizontal"> <!-- Register form -->
            <fieldset>
              <div class="form-group"><!-- Logindetails  -->
                <legend>Create Account</legend>
                <div class="col-lg-12"><!-- Company name -->
                  {!!Form::text('companyName','',array('id'=>'nameInput','class'=>'form-control','placeholder' => 'Company Name'))!!}
                </div>
                <div class="col-lg-12"><!-- Email -->
                  {!!Form::email('email','',array('id'=>'emailinput','class'=>'form-control','placeholder' => 'Email'))!!}
                </div> 
                <div class="col-lg-12"><!-- Password -->
                  {!!Form::password('password',array('id'=>'passwordinput','class'=>'form-control','placeholder' => 'Password'))!!}
                </div>
                <div class="col-lg-12"><!-- Confirm password -->
                  {!!Form::password('cpassword',array('id'=>'cpasswordinput','class'=>'form-control','placeholder' => 'Confirm Password'))!!}
                </div>
              </div><!-- Logindetails end -->
              
              <div class="form-group"><!-- Company address -->
                <label class="control-label" id="companyAddr">Company Address</label>
                <div class="col-lg-12"><!-- Address line 1 -->
                  {!!Form::text('addrline1','',array('id'=>'addrline1input','class'=>'form-control','placeholder' => 'Address Line 1'))!!}
                </div>
                <div class="col-lg-12"><!-- Address line 2 -->
                  {!!Form::text('addrline2','',array('id'=>'addrline2input','class'=>'form-control','placeholder' => 'Address Line 2'))!!}
                </div>
                <div class="col-lg-12"><!-- City -->
                  {!!Form::text('city','',array('id'=>'cityinput','class'=>'form-control','placeholder' => 'City/Town'))!!}
                </div>
                <div class="col-lg-12"><!-- Postcode -->
                  {!!Form::text('postcode','',array('id'=>'postinput','class'=>'form-control','placeholder' => 'Postcode'))!!}
                </div>
              </div><!-- Company address end -->

              <div class="form-group"><!-- Contact details -->
                <label class="control-label">Contact Details</label>
                <div class="col-lg-12"><!-- Phone number -->
                  {!!Form::text('phoneNumber','',array('id'=>'pNumber','class'=>'form-control','placeholder' => 'Phone Number'))!!}
                </div>
                <div class="col-lg-12"><!-- URL -->
                  {!!Form::url('url','',array('id'=>'url','class'=>'form-control','placeholder' => 'Company Website'))!!}
                </div>
              </div><!-- Contact details end -->

              <div class="form-group"><!-- Description -->
                <label class="control-label">Description</label>
                <div class="col-lg-12">
                  {!!Form::textarea('description','',array('id'=>'description','class'=>'form-control','rows'=>'3','placeholder' => 'Description'))!!}
                </div>
              </div><!-- Description end -->
              {!!Form::submit('Register', array('class'=>'btn btn-primary'))!!}
            </fieldset>
            {!! Form::close() !!}
          </form><!-- Register form end -->
        </div>
      </div>
    </div>
  </body>
</html>