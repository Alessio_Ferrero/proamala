<!DOCTYPE html>
<html>
  <head>
    <title>Proamala-HomePage</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100' rel='stylesheet' type='text/css'>
    {!!HTML::script('js/jquery.min.js')!!}
    {!!HTML::style('css/font-awesome.min.css')!!}
    {!!HTML::style('css/bootstrap.min.css')!!}
    {!!HTML::style('css/login.css')!!}
  </head>

  <body>
    <div class="container">
      <!--LOGIN/REGISTER FORM-->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-header">
            <h1 id="header">Welcome Back!</h1>
            @if($errors->any())
            <h4>{{$errors->first()}}</h4>
            @endif
          </div>
        </div>
      </div>
      <div class="row">
        <div class="container col-sm-6 col-sm-offset-3">
          <div class="well bs-component">
            {!! Form::open(array('url' => 'login')) !!}
            <form class="form-horizontal">
              <fieldset>
                <div class="form-group">
                  <label class="formtitles control-label">Email/Username</label>
                  <div class="col-lg-12">
                    {!!Form::text('email',null,array('id'=>'','class'=>'form-control','placeholder' => 'e.g. example@mail.com'))!!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="formtitles control-label">Password</label>
                  <div class="col-lg-12">
                    {!!Form::password('password',array('id'=>'','class'=>'form-control','placeholder' => 'e.g. ..................'))!!}
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-12 col-lg-offset-2">
                    {!!Form::submit('Login',array('class'=>'btn btn-primary'))!!}
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
        

